from resemblyzer import VoiceEncoder, preprocess_wav
from pathlib import Path
import numpy as np
from tqdm import tqdm
fpath = Path("/home/danh/Desktop/zalo_audio_c1/Resemblyzer/audio_data/librispeech_test-other/533/533-1066-0000.flac")
print(fpath)
wav = preprocess_wav(fpath)
from itertools import groupby

encoder = VoiceEncoder()

wav_fpaths = list(Path("audio_data", "librispeech_test-other").glob("**/*.flac"))
print (wav_fpaths)
speaker_wavs = {speaker: list(map(preprocess_wav, wav_fpaths)) for speaker, wav_fpaths in
                groupby(tqdm(wav_fpaths, "Preprocessing wavs", len(wav_fpaths), unit="wavs"),
                        lambda wav_fpath: wav_fpath.parent.stem)}
embeds_a = np.array([encoder.embed_utterance(wavs[0]) for wavs in speaker_wavs.values()])
embeds_b = np.array([encoder.embed_utterance(wavs[1]) for wavs in speaker_wavs.values()])

embed = encoder.embed_utterance(wav)
np.set_printoptions(precision=3, suppress=True)
print(embed)