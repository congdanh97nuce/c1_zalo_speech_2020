import random,os,numpy
import csv
import numpy as geek
from resemblyzer import VoiceEncoder, preprocess_wav
from pathlib import Path
import numpy as np
from tqdm import tqdm
from itertools import groupby
import pandas as pd
from numpy  import array
encoder = VoiceEncoder()

list_data = []
list_label= []
list_name_foder = []
wav_fpaths = list(Path("audio_data", "dataset").glob("**/*.wav"))
str_old_name_folder = ""
index_list_name_folder = 0
for one_wav_path in wav_fpaths:
    print("dang xu li file")
    print(str(index_list_name_folder))
    if (one_wav_path.parts[2] != str_old_name_folder ) :
        str_old_name_folder = one_wav_path.parts[2]
        list_name_foder.append(str_old_name_folder)
        index_list_name_folder = index_list_name_folder + 1

        #check list_name_folder  >= 2 element
        if (len(list_name_foder) >= 2) :
            #30 sameple data in 1 couple file, 1 couple same, 1 couple differrent
            for x in range(30):
                #embed 1 wav random element i
                random_file_in_i = random.choice(os.listdir(Path("audio_data", "dataset/" + str_old_name_folder)))
                wav = preprocess_wav(Path("audio_data", "dataset/" + str_old_name_folder + "/" + random_file_in_i))
                embed_i = encoder.embed_utterance(wav)
                np.set_printoptions(precision=3, suppress=True)
                #end embed

                # embed 1 wav random element i-1
                random_file_in_i_1 = random.choice(os.listdir(Path("audio_data", "dataset/" + list_name_foder[index_list_name_folder - 1])))
                wav = preprocess_wav(
                    Path("audio_data", "dataset/" + list_name_foder[index_list_name_folder - 1] + "/" + random_file_in_i_1))
                embedi_i_1 = encoder.embed_utterance(wav)
                np.set_printoptions(precision=3, suppress=True)
                #end embed

                #add different audio embed to data and add label 0
                #out_arr = geek.add(embed_i, embedi_i_1)
                out_arr = geek.subtract(embed_i, embedi_i_1)
                list_data.append(np.absolute(out_arr))
                list_label.append(0)
                #end

                # embed 1 wav random element i same folder
                random_file_in_i = random.choice(
                    os.listdir(Path("audio_data", "dataset/" + str_old_name_folder)))
                wav = preprocess_wav(
                    Path("audio_data", "dataset/" + str_old_name_folder + "/" + random_file_in_i))
                embed_i_same = encoder.embed_utterance(wav)
                np.set_printoptions(precision=3, suppress=True)
                #end embed

                # add same audio embed to data and add label 1
                #out_arr = geek.add(embed_i, embedi_i_1)
                out_arr = geek.subtract(embed_i, embed_i_same)
                list_data.append(np.absolute(out_arr))
                list_label.append(1)
                #end

print (list_name_foder)
print (len(list_name_foder))
print (list_name_foder[0])


a = np.asarray(list_data)
my_df = pd.DataFrame(a)
my_df.to_csv('data_train.csv', index=False)

a1 = np.asarray(list_label)
# a_trans = a1.T
# a_trans1 = array(list_label)

#my_df_a = pd.DataFrame(a1.reshape(-1, len(a1)))
my_df_a = pd.DataFrame(a1)

# my_df_test = pd.DataFrame(np.array(list_label))
my_df_a.to_csv('labels.csv', index=False)


training_dataset = pd.read_csv("data_train.csv")
label = pd.read_csv("labels.csv")
print(training_dataset)
k=training_dataset


list_data1 = list_data