from keras.models import Sequential
from keras.layers import Dense, Dropout
import numpy as np
from sklearn.datasets.samples_generator import make_blobs
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
import numpy as geek
import pandas as pd



from resemblyzer import VoiceEncoder, preprocess_wav
from pathlib import Path




# df_public_test_1 = pd.read_csv("labels_1.csv")
# buckets = [1] * 23880
# df_public_test_1 = pd.DataFrame(buckets)
# df_public_test_1.to_csv('labels_1.csv', index=False)

# data1, labels_1 = make_blobs(n_samples=38, centers=2, n_features=256 ,random_state=0)
# x_tr_1, x_ts_1, y_tr_1, y_ts_1 = train_test_split (data1, labels_1, test_size=0.2)
# plt.scatter(data[:,0], data[:,1], c=labels)
# k = data[0]
# out_arr = geek.subtract(data[0], data[1])
# print(k)

# a = np.asarray(data)
# my_df = pd.DataFrame(data)
# my_df.to_csv('foo.csv', index=False)
# pd.DataFrame(data).to_csv("testfile1.csv")

training_dataset = pd.read_csv("data_train.csv")

training_dataset = np.asarray(training_dataset)

labels = pd.read_csv("labels.csv")

#convert datafame
labels = np.asarray(labels)
labels = labels.ravel()
# labels = np.array(labels)
print(training_dataset)


#arr = numpy.array(list_data)


print(labels.shape)

x_tr, x_ts, y_tr, y_ts = train_test_split (training_dataset, labels, test_size=0.2)

# my_df = pd.DataFrame(labels_1)
# my_df.to_csv('abcc.csv', index=False)


model = Sequential()
model.add(Dense(1, activation='sigmoid', input_dim=256))
model.compile(optimizer='SGD',
 loss='binary_crossentropy',
 metrics=['accuracy'])
model.fit(x_tr, y_tr, epochs=10  , batch_size=64)

# evaluate the model
scores = model.evaluate(x_ts, y_ts)
for i in range(len(scores)):
 print("\n%s: %.2f%%" % (model.metrics_names[i], scores[i]*100))

weights = model.layers[0].get_weights()[0]
biases = model.layers[0].get_weights()[1]
x = np.linspace(np.amin(training_dataset[:,:1]),np.amax(training_dataset[:,:1]))

plt.figure()
a = -(weights[0]/weights[1])/(weights[0]/biases[0])
b = -weights[0]/weights[1]

plt.rcParams["figure.figsize"] = (15,5)
plt.title('iteration '+str(i))
plt.plot(x, [a*i + b for i in x], color='green')
plt.scatter(training_dataset[:, 0], training_dataset[:, 1], c=labels, marker='o',s=20)


##
encoder = VoiceEncoder()

#get data public test
df_public_test = pd.read_csv("public-test.csv")

#print(df_public_test['audio_1'][0])

#index_element_of_row_audio = 0
output_label=[]
#for i in df_public_test['audio_1']:
for index, row in df_public_test.iterrows():
  #name_audio_1 = df_public_test['audio_1'][index_element_of_row_audio]
  # name_audio_1 = row['audio_1']
  # name_audio_2 = row['audio_2']
  #print(str(index_element_of_row_audio))
  fpath = Path("/home/danh/Desktop/Train-Test-Data/public-test/" + row['audio_1'])
  wav_audio_1 = preprocess_wav(fpath)
  embed_audio_1 = encoder.embed_utterance(wav_audio_1)
  np.set_printoptions(precision=3, suppress=True)
  #print(name_audio_1)

  #fpath = Path("/home/danh/Desktop/Train-Test-Data/public-test/" + df_public_test['audio_2'][index_element_of_row_audio])
  fpath = Path("/home/danh/Desktop/Train-Test-Data/public-test/" + row['audio_2'])
  wav_audio_2 = preprocess_wav(fpath)
  embed_audio_2 = encoder.embed_utterance(wav_audio_2)
  np.set_printoptions(precision=3, suppress=True)
  #print(df_public_test['audio_2'][index_element_of_row_audio])

  # arr_add_vector = geek.subtract(embed_audio_1, embed_audio_2)
  #
  # vector_output = np.asarray(np.absolute(arr_add_vector), dtype=float)
  # vector_output = np.array([vector_output])

  # x = np.random.rand(1, 256)
  # kk=x[0]
  # print(type(x[0]))
  # print(type(array[0]))


  #so sanh thuan


  utt_sim_matrix = np.inner(embed_audio_1, embed_audio_2)
  print(utt_sim_matrix)

  #prediction = model.predict(vector_output)
  #y_classes = prediction.argmax(axis=-1)
  #print(type(y_classes[0]))
  #print(prediction)
  #print(y_classes)
  #output_label.append(y_classes[0])
  if (utt_sim_matrix >=0.9) :
    output_label.append(1)
    print("add label 1")
  else:
    print("add label 0")
    output_label.append(0)

  #index_element_of_row_audio = index_element_of_row_audio + 1

df_public_test['labels'] = np.asarray(output_label)

#df = pd.df_public_test
df_public_test.to_csv('submit.csv', index=False)


# df_public_test_1 = pd.read_csv("public-test-1.csv")
# buckets = [0] * 100
# df_public_test_1['labels'] = np.asarray(buckets)
# df_public_test_1.to_csv('submit_1.csv', index=False)

